﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LogiscitsApp.Models
{
    public class ContactModel
    {
        /*
        public List<ContactModel> GetContact()
        {
            using (var context=new LogisticApp_DbEntities())
            {
                var result = context.Contacts.Select(x => new ContactModel()
                {
                    countryID = x.ContactID,
                    email=x.email,
                    address=x.address,
                    city=x.city,
                    zipcode=x.zipcode,
                    timezone=x.timezone,


                });
                
            } 
        }
        */
        public int userID { get; set; }
        public string email { get; set; }
        public Nullable<int> contactID { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public Nullable<System.DateTime> timezone { get; set; }
        public string mobilephone { get; set; }
        public string officephone { get; set; }
        public string fax { get; set; }
        public string note { get; set; }
        public Nullable<int> countryID { get; set; }
        public Nullable<int> stateID { get; set; }
        public Nullable<int> companyID { get; set; }

        public virtual Company Company { get; set; }
        public virtual User User { get; set; }
        public virtual Country Country { get; set; }
        public virtual State State { get; set; }
        public virtual User User1 { get; set; }

        public enum searchByType
        {
            city, address, email, zipcode, mobilephone, officephone, companyName, countryName, stateCode
        }


    }
}