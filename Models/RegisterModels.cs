﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LogiscitsApp.Models
{
  // [MetadataType(typeof(RegisterMetadata))]
    public  class RegisterModels
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = " Email id is required")]
        [Display(Name = "Email Address")]
        public string email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = " password is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [MinLength(6, ErrorMessage = "Minum 6 characters requires")]
        public string password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "confirm password is required")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "passwprd and confirm password should be same")]
        [Display(Name = "Confirmpassword")]
        public string Confirmpassword { get; set; }

        [Display(Name = "Address")]
        public string address { get; set; }

        [Display(Name = "city")]
        public string city { get; set; }

        public string state { get; set; }
        [Display(Name = "zipcode")]

        public string zipcode { get; set; }
        [Display(Name = "time Zone")]
        public Nullable<System.DateTime> timezone { get; set; }

        [Display(Name = "Mobile Phone")]
        public string mobilephone { get; set; }

        [Display(Name = "Office Phone")]
        public string officephone { get; set; }

        [Display(Name = "Fax Number:")]
        public string fax { get; set; }

        public int roleID { get; set; }
        public string roleName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

        // public virtual ICollection<User> Users { get; set; }
        //  public string SuccesMessage;

        //public int roleID { get; set; }

        // public Nullable<int> countryID { get; set; }
        //public Nullable<int> stateID { get; set; } 
    }

    //public class RegisterMetadata
    //{

    // }

}