﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using LogiscitsApp.Models;
using System.Web.Security;


namespace LogiscitsApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                SummitworksProjectEntities2 db = new SummitworksProjectEntities2();
                var user = (from userlist in db.Users
                            where userlist.email == login.UserName && userlist.password == login.Password
                            select new
                            {
                                userlist.email,
                                userlist.password,
                                userlist.userID
                            }).ToList();
                if (user.FirstOrDefault() != null)
                {
                    FormsAuthentication.SetAuthCookie(login.UserName, false);
                    Session["UserName"] = user.FirstOrDefault().email;
                    Session["Password"] = user.FirstOrDefault().password;
                    Session["UserId"] = user.FirstOrDefault().userID;
                    return Redirect("/Contacts/Index");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid login credentials.");
                }
            }
            return View();
        }
        public ActionResult Logout()
        {

            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
        public ActionResult WelcomePage()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Register()
        {
            SummitworksProjectEntities2 db = new SummitworksProjectEntities2();
            RegisterModels objuserModel = new RegisterModels();
            ViewBag.roleId = new SelectList(db.Roles, "roleID", "roleName");
            return View(objuserModel);
        }
        /*

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Exclude = "IsEmailVerified,ActivationCode")]User user)

        {
            bool status = false;
            string message = "";
            if (ModelState.IsValid)
            {
                #region//emal is already exist
                var isExist = IsEmailExit(user.email);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exost");
                    return View(user);
                }
                #endregion
                #region //generate activation code
                user.ActivationCode = Guid.NewGuid();
                #endregion
                #region//password hashing

                user.password = Crypto.Hash(user.password);
                //  user.Confirmpassword = Crypto.Hash(user.Confirmpassword);
                user.IsEmailVerified = false;
                #endregion
                #region//save to DB
                using (LogisticApp_DbEntities dc = new LogisticApp_DbEntities())
                {
                    dc.Users.Add(user);
                    dc.SaveChanges();
                    //send email to user
                    SendVerificationEmail(user.email, user.ActivationCode.ToString());
                    message = "registration successfully done.Account activation link" +
                        "has been sent to your email id:" + user.email;
                    status = true;
                }
                #endregion
            }
            else
            {
                message = "invalid request";
            } 

            ViewBag.message = message;
            ViewBag.Status = status;
            return View(user);

        }

        [NonAction]
        public bool IsEmailExit(string email)
        {
            using (LogisticApp_DbEntities dc = new LogisticApp_DbEntities())
            {
                var v = dc.Users.Where(a => a.email == email).FirstOrDefault();
                return v != null;
            }
        }
    

        [NonAction]
        public void SendVerificationEmail(string email, string activationCode)
        {
            var verifyUrl = "/User/verifyAccount"  + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromemail = new MailAddress("mdpabel15@gmail.com");
            var toemail = new MailAddress(email);
            var fromEmailPassword = "Pabel$153246";//put your actual password

            string subject = "your account is successfully created";
            string body = "<br/><br/> we are excited to tell you that your account is "+
                "successfully created. please click on the below link to verify your account"
                +"<br/><br/><a href='"+link+"'>"+link+"</a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromemail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromemail, toemail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);

        }
        *///registration with email validation


        
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModels objuserModel)
        {
            if (ModelState.IsValid)
            {
                SummitworksProjectEntities2 db = new SummitworksProjectEntities2();
                if (!db.Users.Any(m => m.email == objuserModel.email))
                {
                    User objUser = new Models.User();
                    objUser.email = objuserModel.email;
                    objUser.password = objuserModel.password;
                    objUser.address = objuserModel.address;
                    //objUser.country = objuserModel.country;
                    objUser.city = objuserModel.city;
                    //objUser.state = objuserModel.state;
                    objUser.zipcode = objuserModel.zipcode;
                    objUser.mobilephone = objuserModel.mobilephone;
                    objUser.officephone = objuserModel.officephone;
                    objUser.fax = objuserModel.fax;
                   // ViewBag.userID = new SelectList(db.Roles, "roleID", "roleName", objUser.roleID);

                    db.Users.Add(objUser);
                    db.SaveChanges();
                    ModelState.Clear();
                    return RedirectToAction("Login", "Home");
                }
             
            }
          
            return View();


            // return View(objuserModel);
        }


        //Forget and Reset Password***********************************************************************
        /*
         2) Go to https://www.google.com/settings/security/lesssecureapps

Google requires the account to have "Less Secure Apps" enabled before it can allow the
        */


        //Code that has to be added to Home Controller
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/Home/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("mdpabel15@gmail.com");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "Md$102030";
            

            string subject = "";
            string body = "";
            if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/>br/>We got request for reset your account password. Please click on the below link to reset your password" +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,

                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("mdpabel15@gmail.com", "Md$102030")


            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
        [HttpPost]
        public ActionResult ForgotPassword(string EmailID)
        {

            string message = "";
            bool status = false;

            using (SummitworksProjectEntities2 dc = new SummitworksProjectEntities2())
            {
                var account = dc.Users.Where(a => a.email == EmailID).FirstOrDefault();
                if (account != null)
                {

                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.email, resetCode, "ResetPassword");
                    account.ResetPasswordCode = resetCode;

                    dc.Configuration.ValidateOnSaveEnabled = false;
                    dc.SaveChanges();
                    message = "Reset password link has been sent to your email id.";
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return View();
        }
        public ActionResult ResetPassword(string id)
        {

            using (SummitworksProjectEntities2 dc = new SummitworksProjectEntities2())
            {
                var user = dc.Users.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (SummitworksProjectEntities2 dc = new SummitworksProjectEntities2())
                {
                    var user = dc.Users.Where(a => a.ResetPasswordCode == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.password = model.NewPassword;
                        user.ResetPasswordCode = "";
                        dc.Configuration.ValidateOnSaveEnabled = false;
                        dc.SaveChanges();
                        message = "New password updated successfully";
                    }
                }
            }
            else
            {
                message = "Something invalid";
            }
            ViewBag.Message = message;
          //  return View(model);
            return RedirectToAction("Index", "Contacts");
        }

        //change password
        [Authorize]
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (SummitworksProjectEntities2 dc = new SummitworksProjectEntities2())
                {
                    var user = dc.Users.Where(a => a.password == model.OldPassword).FirstOrDefault();
                    if (user != null)
                    {
                        user.password = model.NewPassword;
                        dc.SaveChanges();
                        message = "New password updated successfully";
                        return RedirectToAction("Login", "Home");
                    }
                }
            }
            else
            {
                message = "Something invalid";
            }
            ViewBag.Message = message;
            return View(model);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
    
