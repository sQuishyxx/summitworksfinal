﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LogiscitsApp.Models;

namespace LogiscitsApp.Controllers
{
     [Authorize]
    public class ContactsController : Controller
    {
        private SummitworksProjectEntities2 db = new SummitworksProjectEntities2();
        // GET: Contacts

        public List<string> retreiveSQL(string queryString)
        {
            List<string> ret = new List<string>();

            using (SqlConnection connection = new SqlConnection("Data Source=DESKTOP-3QCSR4E\\SQLEXPRESS;Initial Catalog=SummitworksProject2;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework"))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                //command.CommandText = queryString;
                SqlDataReader reader = command.ExecuteReader();

                // Call Read before accessing data.

                while (reader.Read())
                {
                    string temp = "";
                    temp += (((IDataRecord)reader)[0]);
                    ret.Add(temp);
                }

                // Call Close when done reading.
                reader.Close();

            }
            return ret;
        }
        public ActionResult Index(string search, ContactModel model, string dropdown)
        {

            if (dropdown == "city")
            {
                return View(db.Contacts.Where(x => x.city.StartsWith(search) || search == null).ToList());
            }
            else if (dropdown == "address")
            {
                return View(db.Contacts.Where(x => x.address.StartsWith(search) || search == null).ToList());
            }
            else if (dropdown == "email")
            {
                return View(db.Contacts.Where(x => x.email.StartsWith(search) || search == null).ToList());
            }
            else if (dropdown == "zipcode")
            {
                return View(db.Contacts.Where(x => x.zipcode.StartsWith(search) || search == null).ToList());
            }
            else if (dropdown == "mobilephone")
            {
                return View(db.Contacts.Where(x => x.mobilephone.StartsWith(search) || search == null).ToList());
            }
            else if (dropdown == "officephone")
            {
                return View(db.Contacts.Where(x => x.officephone.StartsWith(search) || search == null).ToList());
            }
            else if (dropdown == "companyName")
            {
                string queryString = "SELECT DISTINCT companyID FROM Company WHERE companyName like  \'" + search + "%\'";

                List<string> companyID = retreiveSQL(queryString);
                if (search.Length > 0)
                {
                    List<Contact> c = new List<Contact>();
                    for (int i = 0; i < companyID.Count; i++)
                    {
                        int cID = Convert.ToInt32(companyID[i]);
                        var companies = db.Contacts.Where(x => x.companyID == cID).ToList();
                        c.AddRange(companies);
                    }
                    return View(c);
                }

            }
            else if (dropdown == "countryName")
            {
                string queryString = "SELECT DISTINCT countryID FROM Country WHERE countryName like  \'" + search + "%\'";

                List<string> countryID = retreiveSQL(queryString);
                if (search.Length > 0)
                {
                    List<Contact> c = new List<Contact>();
                    for (int i = 0; i < countryID.Count; i++)
                    {
                        int cID = Convert.ToInt32(countryID[i]);
                        var countries = db.Contacts.Where(x => x.countryID == cID).ToList();
                        c.AddRange(countries);
                    }
                    return View(c);
                }
            }
            else if (dropdown == "stateCode")
            {
                string queryString = "SELECT DISTINCT stateID FROM State WHERE stateCode like  \'" + search + "%\'";

                List<string> statecode = retreiveSQL(queryString);
                if (search.Length > 0)
                {
                    List<Contact> c = new List<Contact>();
                    for (int i = 0; i < statecode.Count; i++)
                    {
                        int sID = Convert.ToInt32(statecode[i]);
                        var states = db.Contacts.Where(x => x.stateID == sID).ToList();
                        c.AddRange(states);
                    }
                    return View(c);
                }
            }

            var contacts = db.Contacts.Include(c => c.Company).Include(c => c.User).Include(c => c.Country).Include(c => c.State).Include(c => c.User1);
            return View(contacts.ToList());

        }



        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // GET: Contacts/Create
        [Authorize(Roles = "Administrator,mdhasan@gmail.com")]
        public ActionResult Create()
        {
            ViewBag.companyID = new SelectList(db.Companies, "companyID", "companyName");
            ViewBag.contactID = new SelectList(db.Users, "userID", "email");
            ViewBag.countryID = new SelectList(db.Countries, "countryID", "countryName");
            ViewBag.stateID = new SelectList(db.States, "stateID", "stateCode");
            ViewBag.userID = new SelectList(db.Users, "userID", "email");
            return View();
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Administrator,mdhasan@gmail.com")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "userID,email,contactID,address,city,zipcode,timezone,mobilephone,officephone,fax,note,countryID,stateID,companyID")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Contacts.Add(contact);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.companyID = new SelectList(db.Companies, "companyID", "companyName", contact.companyID);
            ViewBag.contactID = new SelectList(db.Users, "userID", "email", contact.contactID);
            ViewBag.countryID = new SelectList(db.Countries, "countryID", "countryName", contact.countryID);
            ViewBag.stateID = new SelectList(db.States, "stateID", "stateCode", contact.stateID);
            ViewBag.userID = new SelectList(db.Users, "userID", "email", contact.userID);
            return View(contact);
        }

        // GET: Contacts/Edit/5

        [Authorize(Roles = "Administrator,mdhasan@gmail.com")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            ViewBag.companyID = new SelectList(db.Companies, "companyID", "companyName", contact.companyID);
            ViewBag.contactID = new SelectList(db.Users, "userID", "email", contact.contactID);
            ViewBag.countryID = new SelectList(db.Countries, "countryID", "countryName", contact.countryID);
            ViewBag.stateID = new SelectList(db.States, "stateID", "stateCode", contact.stateID);
            ViewBag.userID = new SelectList(db.Users, "userID", "email", contact.userID);
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        [Authorize(Roles = "Administrator,mdhasan@gmail.com")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "userID,email,contactID,address,city,zipcode,timezone,mobilephone,officephone,fax,note,countryID,stateID,companyID")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.companyID = new SelectList(db.Companies, "companyID", "companyName", contact.companyID);
            ViewBag.contactID = new SelectList(db.Users, "userID", "email", contact.contactID);
            ViewBag.countryID = new SelectList(db.Countries, "countryID", "countryName", contact.countryID);
            ViewBag.stateID = new SelectList(db.States, "stateID", "stateCode", contact.stateID);
            ViewBag.userID = new SelectList(db.Users, "userID", "email", contact.userID);
            return View(contact);
        }

        // GET: Contacts/Delete/5

        [Authorize(Roles = "Administrator,mdhasan@gmail.com")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Delete/5
        [Authorize(Roles = "Administrator,mdhasan@gmail.com")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = db.Contacts.Find(id);
            db.Contacts.Remove(contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
